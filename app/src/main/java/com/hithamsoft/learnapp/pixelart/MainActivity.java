package com.hithamsoft.learnapp.pixelart;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public static final int PICK_IMAGE=102;
    private static final int PERMISSION_REQUEST_CODE = 1003;
    private static final String TAG ="MainActivity" ;
    ImageView selectedImage,convertedImage;
    ImageButton uploadImage;
    Button convertButton;
    LinearLayout convertContainer;
    EditText columnsNum,rowNum;
    FrameLayout grid_view;
    GridNet gridNet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        uploadImage.setOnClickListener(this);
        convertButton.setOnClickListener(this);
        gridNet=new GridNet(this);



    }

    private void init(){
        selectedImage=findViewById(R.id.imageSelected_IV);
        convertedImage=findViewById(R.id.imageConverted_IV);
        uploadImage=findViewById(R.id.uploadImage_btn);
        convertButton=findViewById(R.id.convert_btn);
        convertContainer=findViewById(R.id.convert_Container);
        columnsNum=findViewById(R.id.columns_txt);
        rowNum=findViewById(R.id.row_txt);
        grid_view=findViewById(R.id.grid_view);
        findViewById(R.id.removeView_btn).setOnClickListener(this);
        //listener callback

    }

    private void setUploadImage(){
        Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto , PICK_IMAGE);
    }

    //set Grid in image
    private void setGridNet(){
        String columns_num=columnsNum.getText().toString();
        String row_num=rowNum.getText().toString();
        if (columns_num.isEmpty()||row_num.isEmpty()){
            Toast.makeText(this, "Enter columns and row", Toast.LENGTH_SHORT).show();
        }else {
//            PixelGridView pixelGrid = new PixelGridView(this);
//


        gridNet.setImageView(selectedImage);

            grid_view.removeView(gridNet);
            gridNet.setNumColumns(Integer.parseInt(columns_num));
            gridNet.setNumRows(Integer.parseInt(row_num));
            grid_view.addView(gridNet);
            gridNet.invalidate();
            grid_view.invalidate();


        }
    }

    private boolean checkPermission() {
        int resultReadStorage = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        int resultWriteStorage = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);

        return resultReadStorage == PackageManager.PERMISSION_GRANTED && resultWriteStorage == PackageManager.PERMISSION_GRANTED;
    }
    private void requestPermission() {

        ActivityCompat.requestPermissions(this, new String[]{READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {
            switch (requestCode) {

                case PICK_IMAGE:
                    if (resultCode == RESULT_OK && data != null) {
                        Uri selectedImage_Url = data.getData();
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
                        if (selectedImage_Url!= null) {
                            Cursor cursor = getContentResolver().query(selectedImage_Url,
                                    filePathColumn, null, null, null);
                            if (cursor != null) {
                                cursor.moveToFirst();

                                //hide-show item
                                uploadImage.setVisibility(View.GONE);
                                selectedImage.setVisibility(View.VISIBLE);
                                convertContainer.setVisibility(View.VISIBLE);
                                grid_view.setVisibility(View.VISIBLE);

                                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                                String picturePath = cursor.getString(columnIndex);
                                selectedImage.setImageBitmap(BitmapFactory.decodeFile(picturePath));
                                cursor.close();
                            }
                        }

                    }
                    break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {

                    boolean writeStorage = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean readStorage = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    if (readStorage)
                        setUploadImage();
                    else {

                        Toast.makeText(this, "Permission dined", Toast.LENGTH_SHORT).show();
                    }
                }


                break;
        }
    }

    @Override
        public void onClick (View v){
            if (v.getId() == uploadImage.getId()) {
                Log.d(TAG, "onClick: upload image button");
                if (checkPermission()){
                    setUploadImage();
                }else {
                    requestPermission();
                }

            } else if (v.getId() == convertButton.getId()) {
                setGridNet();
            }else if (v.getId()==R.id.removeView_btn){
                grid_view.removeView(gridNet);
            }

        }
    }
