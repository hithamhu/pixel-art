package com.hithamsoft.learnapp.pixelart;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

public class GridNet extends View {

//    private int rowsCount = 8;
//    private int columnsCount = 8;

    private int numColumns, numRows;
    private int cellWidth, cellHeight;
    private Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    ImageView imageView;

    public GridNet(Context context) {
        super(context);
        paint.setColor(Color.WHITE);
    }
    public void setImageView(ImageView imageView){
        this.imageView=imageView;
    }

    private int getimageHeight(){
       return imageView.getHeight();
    }
    private int getimageWidth(){
        return imageView.getWidth();
    }

    public void setNumColumns(int numColumns) {
        this.numColumns = numColumns;
        calculateDimensions();
    }

    public int getNumColumns() {
        return numColumns;
    }

    public void setNumRows(int numRows) {
        this.numRows = numRows;
        calculateDimensions();
    }

    public int getNumRows() {
        return numRows;
    }

    private void calculateDimensions() {
        if (numColumns < 1 || numRows < 1) {
            return;
        }

        cellWidth = getWidth() / numColumns;
        cellHeight = getHeight() / numRows;

//        cellChecked = new boolean[numColumns][numRows];

        invalidate();
    }




    @Override
    protected void onDraw(Canvas canvas) {
        invalidate();
        int height = getimageHeight();
        int width = getimageWidth();
        for (int i = 0; i < numRows; ++i) {
            canvas.drawLine(0, height / numRows * (i + 1), width, height / numRows * (i + 1), paint);
        }
        for (int i = 0; i < getNumColumns(); ++i) {
            canvas.drawLine(width / numColumns * (i + 1), 0, width / numColumns * (i + 1), height, paint);
        }
        super.onDraw(canvas);
    }

}